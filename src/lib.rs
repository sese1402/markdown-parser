use render::html::RenderHTML;

pub mod ast;
pub mod ast_elements;
pub mod text_block;

pub mod render;

pub mod error;

#[cfg(test)]
pub mod tests;

type TextFormatInnerElement = Vec<TextFormat>;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct AST {
    pub root: Vec<ASTElement>,
}

/// ``` rust
/// use CorrosionMark::parse_to_ast_model;
///
/// fn ast_element () {
///     let input: &str = "My Text *testing* ";
///
///     print!("{:?}", parse_to_ast_model(input));
/// }
/// ```
///
/// This function generates an AST tree.
pub fn parse_to_ast_model(input: &str) -> AST {
    AST::parser(input)
}

/// ``` rust
/// use CorrosionMark::ast_to_html;
///
/// fn md_convert_to_html () {
///     let input: &str = "My Text *testing* ";
///
///     print!("{:?}", ast_to_html(input));
/// }
/// ```
///
/// This function generates from md to html.
pub fn md_to_html(input: &str) -> Result<String, error::CorrError> {
    let ast_model = AST::parser(input);
    AST::render(&ast_model)
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ASTElement {
    Heading {
        text: FormattedTextBlock,
        tier: u8,
    },
    TextBlock {
        text: FormattedTextBlock,
    },
    Liste {
        elements: Vec<ListItem>,
    },
    Image {
        link_text: Option<String>,
        link_url: Option<String>,
        image_alt: Option<String>,
        image_url: String,
    },
    Table {
        headers: Vec<FormattedTextBlock>,
        inhalt: Vec<Vec<FormattedTextBlock>>,
    },
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ListItemType {
    Ordered(usize),
    Unordered,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ListItem {
    pub item_type: ListItemType,
    pub level: usize,
    pub content: FormattedTextBlock,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct FormattedTextBlock {
    pub inhalt: Vec<TextFormat>,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum TextFormat {
    Bold(TextFormatInnerElement),
    Italic(TextFormatInnerElement),
    StrikeThrough(TextFormatInnerElement),

    Link {
        name: TextFormatInnerElement,
        href: String,
    },
    Text(String),
}

use core::fmt;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum CorrError {
    #[error("write macro throws error")]
    WritError(#[from] fmt::Error),
}

use crate::TextFormat;

#[test]
fn italic_asterik() {
    let (rest, parsed) = TextFormat::parse_italic("*test*").unwrap();
    assert_eq!(
        parsed,
        TextFormat::Italic(vec![TextFormat::Text("test".to_owned())])
    );
    assert_eq!(rest, "");
}

#[test]
fn italic_underscore() {
    let (rest, parsed) = TextFormat::parse_italic("_test_").unwrap();
    assert_eq!(
        parsed,
        TextFormat::Italic(vec![TextFormat::Text("test".to_owned())])
    );
    assert_eq!(rest, "");
}

#[test]
#[should_panic]
fn italic_false_underscore() {
    TextFormat::parse_italic("_test").unwrap();
}

#[test]
#[should_panic]
fn italic_false_asterik() {
    TextFormat::parse_italic("*test").unwrap();
}

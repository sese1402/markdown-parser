use crate::{
    render::html::RenderHTML, ASTElement, FormattedTextBlock, ListItem, ListItemType, TextFormat,
};

#[test]
fn bold() {
    let input = TextFormat::Bold(vec![TextFormat::Text("test".to_owned())]);

    assert_eq!("<b>test</b>", TextFormat::render(&input).unwrap());
}

#[test]
fn italic() {
    let input = TextFormat::Italic(vec![TextFormat::Text("test".to_owned())]);

    assert_eq!("<i>test</i>", TextFormat::render(&input).unwrap());
}

#[test]
fn strike_through() {
    let input = TextFormat::StrikeThrough(vec![TextFormat::Text("test".to_owned())]);

    assert_eq!("<s>test</s>", TextFormat::render(&input).unwrap());
}

#[test]
fn plain_text() {
    let input = TextFormat::Text("test".to_owned());

    assert_eq!("test", TextFormat::render(&input).unwrap());
}

#[test]
fn link() {
    let input = TextFormat::Link {
        href: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned(),
        name: vec![TextFormat::Text("test".to_owned())],
    };

    assert_eq!(
        "<a href=\" https://www.youtube.com/watch?v=dQw4w9WgXcQ \">test</a>",
        TextFormat::render(&input).unwrap()
    );
}

#[test]
fn formatted_text_block() {
    let input = ASTElement::TextBlock {
        text: FormattedTextBlock {
            inhalt: vec![TextFormat::Text("test".to_owned())],
        },
    };

    assert_eq!("<p>test</p>", ASTElement::render(&input).unwrap())
}

#[test]
fn header() {
    let input = ASTElement::Heading {
        tier: 1,
        text: FormattedTextBlock {
            inhalt: vec![TextFormat::Text("test".to_owned())],
        },
    };

    assert_eq!("<h1>test</h1>", ASTElement::render(&input).unwrap());
}

#[test]
fn image() {
    let input = ASTElement::Image {
        link_url: None,
        link_text: None,
        image_alt: Some("AltText".to_owned()),
        image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned(),
    };
    assert_eq!("<a href=\"\"><img src=\"https://www.youtube.com/watch?v=dQw4w9WgXcQ\" alt=\"AltText\"></a>", ASTElement::render(&input).unwrap());
}

#[test]
fn image_with_link() {
    let input = ASTElement::Image {
        link_url: Some("link_url".to_owned()),
        link_text: Some("link_text".to_owned()),
        image_alt: Some("image_alt".to_owned()),
        image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned(),
    };
    assert_eq!("<a href=\"link_url\"><img src=\"https://www.youtube.com/watch?v=dQw4w9WgXcQ\" alt=\"image_alt\">link_text</a>", ASTElement::render(&input).unwrap());
}

#[test]
fn liste() {
    let input = ASTElement::Liste {
        elements: vec![
            ListItem {
                item_type: ListItemType::Ordered(1),
                level: 0,
                content: FormattedTextBlock {
                    inhalt: vec![TextFormat::Text("test".to_owned())],
                },
            },
            ListItem {
                item_type: ListItemType::Unordered,
                level: 0,
                content: FormattedTextBlock {
                    inhalt: vec![TextFormat::Text("test".to_owned())],
                },
            },
        ],
    };

    assert_eq!(
        "<ul><li>1. test<li><li>- test<li></ul>",
        ASTElement::render(&input).unwrap()
    );
}

#[test]
fn liste_more_layer() {
    let input = ASTElement::Liste {
        elements: vec![
            ListItem {
                item_type: ListItemType::Ordered(1),
                level: 0,
                content: FormattedTextBlock {
                    inhalt: vec![TextFormat::Text("test".to_owned())],
                },
            },
            ListItem {
                item_type: ListItemType::Unordered,
                level: 1,
                content: FormattedTextBlock {
                    inhalt: vec![TextFormat::Text("test".to_owned())],
                },
            },
        ],
    };

    assert_eq!(
        "<ul><li>1. test<li><ul><li>- test<li></ul></ul>",
        ASTElement::render(&input).unwrap()
    );
}

#[test]
fn table_not_impl_message() {
    let input = ASTElement::Table {
        headers: vec![
            FormattedTextBlock {
                inhalt: vec![TextFormat::Text("test1header".to_owned())],
            },
            FormattedTextBlock {
                inhalt: vec![TextFormat::Text("test2header".to_owned())],
            },
        ],
        inhalt: vec![
            vec![
                FormattedTextBlock {
                    inhalt: vec![TextFormat::Text("test1".to_owned())],
                },
                FormattedTextBlock {
                    inhalt: vec![TextFormat::Text("test2".to_owned())],
                },
            ],
            vec![
                FormattedTextBlock {
                    inhalt: vec![TextFormat::Text("test1".to_owned())],
                },
                FormattedTextBlock {
                    inhalt: vec![TextFormat::Text("test2".to_owned())],
                },
            ],
        ],
    };

    assert_eq!(
        "<table><tr></th>test1header</th></th>test2header</th></tr><tr><td>test1</td><td>test2</td><tr><tr><td>test1</td><td>test2</td><tr><table>",
        ASTElement::render(&input).unwrap()
    );
}

use crate::TextFormat;

#[test]
fn link() {
    let (rest, output) =
        TextFormat::parse_link("[test](https://www.markdownguide.org/basic-syntax/#links)")
            .unwrap();

    assert_eq!(
        output,
        TextFormat::Link {
            href: "https://www.markdownguide.org/basic-syntax/#links".to_owned(),
            name: vec![TextFormat::Text("test".to_owned())]
        }
    );
    assert_eq!(rest, "");
}

#[test]
#[should_panic]
fn link_with_space_for_square_brace() {
    TextFormat::parse_link(" [test](https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

#[test]
#[should_panic]
fn link_without_open_square_brace() {
    TextFormat::parse_link("test](https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

#[test]
#[should_panic]
fn link_without_closed_square_brace() {
    TextFormat::parse_link("[test(https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

#[test]
#[should_panic]
fn link_without_open_brace() {
    TextFormat::parse_link("[test]https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

#[test]
#[should_panic]
fn link_without_closed_brace() {
    TextFormat::parse_link("[test](https://www.youtube.com/watch?v=dQw4w9WgXcQ").unwrap();
}

#[test]
#[should_panic]
fn link_with_space_between_squared_brace_and_brace() {
    TextFormat::parse_link("[test] (https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

#[test]
#[should_panic]
fn link_with_empty_brace() {
    TextFormat::parse_link("[test]()").unwrap();
}

#[test]
#[should_panic]
fn link_with_empty_square_brace() {
    TextFormat::parse_link("[](https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

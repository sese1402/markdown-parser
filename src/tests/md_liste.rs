use crate::{ASTElement, FormattedTextBlock, ListItem, ListItemType, TextFormat};

#[test]
fn list_without_indentation() {
    let input = r#"- test
- test"#;

    let (rest, output_left) = ASTElement::parse_list(input).unwrap();

    assert_eq!(
        output_left,
        ASTElement::Liste {
            elements: vec![
                ListItem {
                    item_type: ListItemType::Unordered,
                    level: 0,
                    content: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    },
                },
                ListItem {
                    item_type: ListItemType::Unordered,
                    level: 0,
                    content: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    },
                }
            ]
        }
    );
    assert_eq!(rest, "");
}

#[test]
fn list_with_indentation() {
    let input = r#" - test
- test"#;

    let (rest, output_left) = ASTElement::parse_list(input).unwrap();

    assert_eq!(
        output_left,
        ASTElement::Liste {
            elements: vec![
                ListItem {
                    item_type: ListItemType::Unordered,
                    level: 1,
                    content: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    },
                },
                ListItem {
                    item_type: ListItemType::Unordered,
                    level: 0,
                    content: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    },
                }
            ]
        }
    );
    assert_eq!(rest, "");
}

#[test]
fn list_with_same_numbers() {
    let input = r#"1.test
1.test"#;

    let (rest, output_left) = ASTElement::parse_list(input).unwrap();

    assert_eq!(
        output_left,
        ASTElement::Liste {
            elements: vec![
                ListItem {
                    item_type: ListItemType::Ordered(1),
                    level: 0,
                    content: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    },
                },
                ListItem {
                    item_type: ListItemType::Ordered(1),
                    level: 0,
                    content: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    },
                }
            ]
        }
    );
    assert_eq!(rest, "");
}

#[test]
fn list_with_diffrent_numbers() {
    let input = r#"1.test
10.test"#;

    let (rest, output_left) = ASTElement::parse_list(input).unwrap();

    assert_eq!(
        output_left,
        ASTElement::Liste {
            elements: vec![
                ListItem {
                    item_type: ListItemType::Ordered(1),
                    level: 0,
                    content: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    },
                },
                ListItem {
                    item_type: ListItemType::Ordered(10),
                    level: 0,
                    content: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    },
                }
            ]
        }
    );
    assert_eq!(rest, "");
}

#[test]
fn list_mixed_iteration() {
    let input = r#"1.test
- test"#;

    let (rest, output_left) = ASTElement::parse_list(input).unwrap();

    assert_eq!(
        output_left,
        ASTElement::Liste {
            elements: vec![
                ListItem {
                    item_type: ListItemType::Ordered(1),
                    level: 0,
                    content: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    },
                },
                ListItem {
                    item_type: ListItemType::Unordered,
                    level: 0,
                    content: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    },
                }
            ]
        }
    );
    assert_eq!(rest, "");
}

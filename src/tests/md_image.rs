use crate::ASTElement;

#[test]
fn image() {
    let (rest, output) =
        ASTElement::parse_image("![AltText](https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();

    assert_eq!(
        output,
        ASTElement::Image {
            link_url: None,
            link_text: None,
            image_alt: Some("AltText".to_owned()),
            image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned()
        }
    );
    assert_eq!(rest, "");
}

#[test]
fn image_without_alt() {
    let (rest, output) =
        ASTElement::parse_image("![](https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();

    assert_eq!(
        output,
        ASTElement::Image {
            link_url: None,
            link_text: None,
            image_alt: None,
            image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned()
        }
    );
    assert_eq!(rest, "");
}

#[test]
fn image_with_link() {
    let (rest, output) = ASTElement::parse_image(
        "[link_text![image_alt](https://www.youtube.com/watch?v=dQw4w9WgXcQ)](link_url)",
    )
    .unwrap();

    assert_eq!(
        output,
        ASTElement::Image {
            link_url: Some("link_url".to_owned()),
            link_text: Some("link_text".to_owned()),
            image_alt: Some("image_alt".to_owned()),
            image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned()
        }
    );
    assert_eq!(rest, "");
}

#[test]
fn image_with_link_without_link_text() {
    let (rest, output) = ASTElement::parse_image(
        "[![image_alt](https://www.youtube.com/watch?v=dQw4w9WgXcQ)](link_url)",
    )
    .unwrap();

    assert_eq!(
        output,
        ASTElement::Image {
            link_url: Some("link_url".to_owned()),
            link_text: None,
            image_alt: Some("image_alt".to_owned()),
            image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned()
        }
    );
    assert_eq!(rest, "");
}

#[test]
fn image_with_link_without_image_alt() {
    let (rest, output) = ASTElement::parse_image(
        "[link_text![](https://www.youtube.com/watch?v=dQw4w9WgXcQ)](link_url)",
    )
    .unwrap();

    assert_eq!(
        output,
        ASTElement::Image {
            link_url: Some("link_url".to_owned()),
            link_text: Some("link_text".to_owned()),
            image_alt: None,
            image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned()
        }
    );
    assert_eq!(rest, "");
}

#[test]
fn image_with_link_without_image_alt_and_link_text() {
    let (rest, output) =
        ASTElement::parse_image("[![](https://www.youtube.com/watch?v=dQw4w9WgXcQ)](link_url)")
            .unwrap();

    assert_eq!(
        output,
        ASTElement::Image {
            link_url: Some("link_url".to_owned()),
            link_text: None,
            image_alt: None,
            image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned()
        }
    );
    assert_eq!(rest, "");
}

#[test]
#[should_panic]
fn image_without_exclamation_point() {
    ASTElement::parse_image("[test](https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

#[test]
#[should_panic]
fn image_with_space_after_exclamation_point() {
    ASTElement::parse_image("! [test](https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

#[test]
#[should_panic]
fn image_with_space_for_exclamation_point() {
    ASTElement::parse_image(" ![test](https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

#[test]
#[should_panic]
fn image_without_open_square_brace() {
    ASTElement::parse_image("!test](https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

#[test]
#[should_panic]
fn image_without_closed_square_brace() {
    ASTElement::parse_image("![test(https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

#[test]
#[should_panic]
fn image_without_open_brace() {
    ASTElement::parse_image("![test]https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

#[test]
#[should_panic]
fn image_without_closed_brace() {
    ASTElement::parse_image("![test]https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

#[test]
#[should_panic]
fn image_with_space_between_squared_brace_and_brace() {
    ASTElement::parse_image("![test] (https://www.youtube.com/watch?v=dQw4w9WgXcQ)").unwrap();
}

#[test]
#[should_panic]
fn image_with_empty_brace() {
    ASTElement::parse_image("![test]()").unwrap();
}

#[test]
fn image_with_link_without_first_open_scuare_brace() {
    let (rest, output) =
        ASTElement::parse_image("![test](https://www.youtube.com/watch?v=dQw4w9WgXcQ)](link_url)")
            .unwrap();

    assert_eq!(
        output,
        ASTElement::Image {
            link_text: None,
            link_url: None,
            image_alt: Some("test".to_owned()),
            image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned()
        }
    );
    assert_eq!(rest, "](link_url)");
}

#[test]
#[should_panic]
fn image_with_link_without_second_open_square_brace() {
    ASTElement::parse_image("[!test](https://www.youtube.com/watch?v=dQw4w9WgXcQ)](link_url)")
        .unwrap();
}

#[test]
#[should_panic]
fn image_with_link_without_exclamation_point() {
    ASTElement::parse_image("[[test](https://www.youtube.com/watch?v=dQw4w9WgXcQ)](link_url)")
        .unwrap();
}

#[test]
#[should_panic]
fn image_with_link_without_first_closed_square_brace() {
    ASTElement::parse_image("[![test(https://www.youtube.com/watch?v=dQw4w9WgXcQ)](link_url)")
        .unwrap();
}

#[test]
#[should_panic]
fn image_with_link_with_empty_image_link() {
    ASTElement::parse_image("[![test]()](link_url)").unwrap();
}

#[test]
#[should_panic]
fn image_with_link_with_empty_link_url() {
    ASTElement::parse_image("[![test(https://www.youtube.com/watch?v=dQw4w9WgXcQ)]()").unwrap();
}

use crate::TextFormat;

#[test]
fn strike_trough() {
    let (rest, parsed) = TextFormat::parse_strike_through("~~test~~").unwrap();
    assert_eq!(
        parsed,
        TextFormat::StrikeThrough(vec![TextFormat::Text("test".to_owned())])
    );
    assert_eq!(rest, "");
}

#[test]
#[should_panic]
fn strike_trough_one_tilde_false() {
    TextFormat::parse_strike_through("~test").unwrap();
}

#[test]
#[should_panic]
fn strike_trough_two_tilde_false() {
    TextFormat::parse_strike_through("~~test").unwrap();
}

use crate::{ASTElement, FormattedTextBlock, TextFormat};

#[test]
fn table_heading() {
    let input = r#"|test1header|test2header|"#;

    let (rest, output) = ASTElement::parser_heading_table(input).unwrap();

    assert_eq!(
        output,
        vec![
            FormattedTextBlock {
                inhalt: vec![TextFormat::Text("test1header".to_owned())]
            },
            FormattedTextBlock {
                inhalt: vec![TextFormat::Text("test2header".to_owned())]
            },
        ]
    );
    assert_eq!(rest, "");
}

#[test]
fn table_heading_with_whitespaces() {
    let input = r#"| test1header | test2header |"#;

    let (rest, output) = ASTElement::parser_heading_table(input).unwrap();

    assert_eq!(
        output,
        vec![
            FormattedTextBlock {
                inhalt: vec![TextFormat::Text(" test1header ".to_owned())]
            },
            FormattedTextBlock {
                inhalt: vec![TextFormat::Text(" test2header ".to_owned())]
            },
        ]
    );
    assert_eq!(rest, "");
}

#[test]
#[should_panic]
fn table_heading_text_before_first_pipe() {
    let input = r#"test|test1header|test2header|"#;

    ASTElement::parser_heading_table(input).unwrap();
}

#[test]
fn table_second_line() {
    let input = r#"|------|-------|"#;

    ASTElement::parser_second_line_table(input).unwrap();
}

#[test]
#[should_panic]
fn table_second_line_before_first_pipe_text() {
    let input = r#"test|------|-------|"#;

    ASTElement::parser_second_line_table(input).unwrap();
}

#[test]
fn table_inhalt_one_line() {
    let input = r#"|test1|test2|"#;

    let output = ASTElement::parser_inhalt_table(input);

    assert_eq!(
        output,
        Ok((
            "",
            vec![vec![
                FormattedTextBlock {
                    inhalt: vec![TextFormat::Text("test1".to_owned())]
                },
                FormattedTextBlock {
                    inhalt: vec![TextFormat::Text("test2".to_owned())]
                },
            ],],
        ))
    );
}

#[test]
fn table_inhalt_multi_line() {
    let input = "|test1|test2|
|test1|test2|";

    let output = ASTElement::parser_inhalt_table(input);

    assert_eq!(
        output,
        Ok((
            "",
            vec![
                vec![
                    FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test1".to_owned())]
                    },
                    FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test2".to_owned())]
                    },
                ],
                vec![
                    FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test1".to_owned())]
                    },
                    FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test2".to_owned())]
                    },
                ]
            ]
        ))
    );
}

#[test]
fn table() {
    let input = r#"|test1header|test2header|
|-----------|-----------|
|test1|test2|"#;

    let output = ASTElement::parse_table(input);

    assert_eq!(
        output,
        Ok((
            "",
            ASTElement::Table {
                headers: vec![
                    FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test1header".to_owned())]
                    },
                    FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test2header".to_owned())]
                    },
                ],
                inhalt: vec![vec![
                    FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test1".to_owned())]
                    },
                    FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test2".to_owned())]
                    },
                ],],
            }
        ))
    );
}

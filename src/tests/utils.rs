use crate::{ASTElement, FormattedTextBlock, TextFormat};

pub fn builder_content(inhalt: Vec<TextFormat>) -> ASTElement {
    ASTElement::TextBlock {
        text: FormattedTextBlock { inhalt },
    }
}

pub fn builder_header(tier: u8, text: Vec<TextFormat>) -> ASTElement {
    ASTElement::Heading {
        tier,
        text: FormattedTextBlock { inhalt: text },
    }
}

// pub fn builder_list(inhalt: Vec<(Option<usize>, usize, FormattedTextBlock)>) -> ASTElement {
//     ASTElement::Liste {
//         elements: inhalt,
//     }
// }

use crate::*;

#[test]
fn paragraph() {
    let input =
        "Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit enim labore culpa
sint ad nisi Lorem pariatur mollit ex esse exercitation amet.
Nisi anim cupidatat excepteur officia. Reprehenderit nostrud nostrud
ipsum Lorem est aliquip amet voluptate voluptate dolor minim nulla est proident.
Nostrud officia pariatur ut officia. Sit irure elit esse ea nulla sunt ex occaecat
reprehenderit commodo officia dolor Lorem duis laboris cupidatat officia voluptate.
Culpa proident adipisicing id nulla nisi laboris ex in Lorem sunt duis officia eiusmod.
Aliqua reprehenderit commodo ex non excepteur duis sunt velit enim.
Voluptate laboris sint cupidatat ullamco ut ea consectetur et est culpa et culpa duis.";
    let AST { root } = AST::parser(input);
    assert_eq!(root.len(), 1);
    let textblock = root[0].clone();
    match textblock {
        ASTElement::TextBlock { text } => {
            assert_eq!(text.inhalt.len(), 1);
        }
        _ => panic!("This should be a textblock"),
    }
}

#[test]
fn nested_heading() {
    let input = "### testing";

    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![ASTElement::Heading {
                text: FormattedTextBlock {
                    inhalt: vec![TextFormat::Text("testing".to_owned())],
                },
                tier: 3,
            }],
        }
    )
}

#[test]
fn nested_text_heading() {
    let input = "### testing

test";

    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![
                ASTElement::Heading {
                    text: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("testing".to_owned())],
                    },
                    tier: 3,
                },
                ASTElement::TextBlock {
                    text: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    }
                },
            ],
        }
    )
}

#[test]
fn nested_heading_text() {
    let input = "test
### testing";

    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![
                ASTElement::TextBlock {
                    text: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    }
                },
                ASTElement::Heading {
                    text: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("testing".to_owned())]
                    },
                    tier: 3
                }
            ]
        }
    )
}

#[test]
fn empty_input() {
    let input = "";

    let output = AST::parser(input);

    assert_eq!(output, AST { root: vec![] })
}

#[test]
fn input_not_grama() {
    let input = "零";

    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![ASTElement::TextBlock {
                text: FormattedTextBlock {
                    inhalt: vec![TextFormat::Text("零".to_owned())]
                }
            }]
        }
    )
}

#[test]
fn input_image() {
    let input = "![AltText](https://www.youtube.com/watch?v=dQw4w9WgXcQ)";
    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![ASTElement::Image {
                link_text: None,
                link_url: None,
                image_alt: Some("AltText".to_owned()),
                image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned()
            },]
        }
    )
}

#[test]
fn input_image_with_linebreak() {
    let input = "![AltText](https://www.youtube.com/watch?v=dQw4w9WgXcQ)";
    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![ASTElement::Image {
                link_text: None,
                link_url: None,
                image_alt: Some("AltText".to_owned()),
                image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned()
            },]
        }
    )
}

#[test]
fn input_image_text() {
    let input = "![AltText](https://www.youtube.com/watch?v=dQw4w9WgXcQ)test";
    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![
                ASTElement::Image {
                    link_text: None,
                    link_url: None,
                    image_alt: Some("AltText".to_owned()),
                    image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned()
                },
                ASTElement::TextBlock {
                    text: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    }
                }
            ]
        }
    )
}

#[test]
fn input_text_image() {
    let input = "test![AltText](https://www.youtube.com/watch?v=dQw4w9WgXcQ)";
    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![
                ASTElement::TextBlock {
                    text: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    }
                },
                ASTElement::Image {
                    link_text: None,
                    link_url: None,
                    image_alt: Some("AltText".to_owned()),
                    image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned()
                },
            ]
        }
    )
}

#[test]
fn input_text_image_text() {
    let input = "test![AltText](https://www.youtube.com/watch?v=dQw4w9WgXcQ)test";
    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![
                ASTElement::TextBlock {
                    text: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    }
                },
                ASTElement::Image {
                    link_text: None,
                    link_url: None,
                    image_alt: Some("AltText".to_owned()),
                    image_url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned()
                },
                ASTElement::TextBlock {
                    text: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test".to_owned())]
                    }
                }
            ]
        }
    )
}

#[test]
fn input_list() {
    let input = "- test
- test";
    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![ASTElement::Liste {
                elements: vec![
                    ListItem {
                        item_type: ListItemType::Unordered,
                        level: 0,
                        content: FormattedTextBlock {
                            inhalt: vec![TextFormat::Text("test".to_owned())]
                        },
                    },
                    ListItem {
                        item_type: ListItemType::Unordered,
                        level: 0,
                        content: FormattedTextBlock {
                            inhalt: vec![TextFormat::Text("test".to_owned())]
                        },
                    }
                ]
            }]
        }
    )
}

#[test]
fn input_list_with_line_break() {
    let input = "- test
- test";
    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![ASTElement::Liste {
                elements: vec![
                    ListItem {
                        item_type: ListItemType::Unordered,
                        level: 0,
                        content: FormattedTextBlock {
                            inhalt: vec![TextFormat::Text("test".to_owned())]
                        },
                    },
                    ListItem {
                        item_type: ListItemType::Unordered,
                        level: 0,
                        content: FormattedTextBlock {
                            inhalt: vec![TextFormat::Text("test".to_owned())]
                        },
                    }
                ]
            }]
        }
    )
}

#[test]
fn input_text_list() {
    let input = "prefixtext
- listitem
- listitem";
    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![
                ASTElement::TextBlock {
                    text: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("prefixtext".to_owned())]
                    }
                },
                ASTElement::Liste {
                    elements: vec![
                        ListItem {
                            item_type: ListItemType::Unordered,
                            level: 0,
                            content: FormattedTextBlock {
                                inhalt: vec![TextFormat::Text("listitem".to_owned())]
                            },
                        },
                        ListItem {
                            item_type: ListItemType::Unordered,
                            level: 0,
                            content: FormattedTextBlock {
                                inhalt: vec![TextFormat::Text("listitem".to_owned())]
                            },
                        }
                    ]
                }
            ]
        }
    )
}

#[test]
fn table() {
    let input = r#"|test1header|test2header|
|-----------|-----------|
|test1|test2|
text"#;

    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![
                ASTElement::Table {
                    headers: vec![
                        FormattedTextBlock {
                            inhalt: vec![TextFormat::Text("test1header".to_owned())]
                        },
                        FormattedTextBlock {
                            inhalt: vec![TextFormat::Text("test2header".to_owned())]
                        },
                    ],
                    inhalt: vec![vec![
                        FormattedTextBlock {
                            inhalt: vec![TextFormat::Text("test1".to_owned())]
                        },
                        FormattedTextBlock {
                            inhalt: vec![TextFormat::Text("test2".to_owned())]
                        },
                    ],],
                },
                ASTElement::TextBlock {
                    text: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("text".to_owned())]
                    }
                },
            ]
        }
    );
}

#[test]
fn table_with_line_break() {
    let input = r#"|test1header|test2header|
|-----------|-----------|
|test1|test2|
"#;

    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![ASTElement::Table {
                headers: vec![
                    FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test1header".to_owned())]
                    },
                    FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test2header".to_owned())]
                    },
                ],
                inhalt: vec![vec![
                    FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test1".to_owned())]
                    },
                    FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("test2".to_owned())]
                    },
                ],],
            },]
        }
    );
}

#[test]
fn input_text_input() {
    let input = r#"text
|test1header|test2header|
|-----------|-----------|
|test1|test2|
"#;

    let output = AST::parser(input);

    assert_eq!(
        output,
        AST {
            root: vec![
                ASTElement::TextBlock {
                    text: FormattedTextBlock {
                        inhalt: vec![TextFormat::Text("text".to_owned())]
                    }
                },
                ASTElement::Table {
                    headers: vec![
                        FormattedTextBlock {
                            inhalt: vec![TextFormat::Text("test1header".to_owned())]
                        },
                        FormattedTextBlock {
                            inhalt: vec![TextFormat::Text("test2header".to_owned())]
                        },
                    ],
                    inhalt: vec![vec![
                        FormattedTextBlock {
                            inhalt: vec![TextFormat::Text("test1".to_owned())]
                        },
                        FormattedTextBlock {
                            inhalt: vec![TextFormat::Text("test2".to_owned())]
                        },
                    ],],
                },
            ]
        }
    );
}

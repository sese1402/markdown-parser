use crate::TextFormat;

#[test]
fn bold_asterik() {
    let (rest, parsed) = TextFormat::parse_bold("**test**").unwrap();
    assert_eq!(
        parsed,
        TextFormat::Bold(vec![TextFormat::Text("test".to_owned())])
    );
    assert_eq!(rest, "");
}

#[test]
fn bold_underscore() {
    let (rest, parsed) = TextFormat::parse_bold("__test__").unwrap();
    assert_eq!(
        parsed,
        TextFormat::Bold(vec![TextFormat::Text("test".to_owned())])
    );
    assert_eq!(rest, "");
}

#[test]
#[should_panic]
fn bold_false_underscore() {
    TextFormat::parse_bold("__test").unwrap();
}

#[test]
#[should_panic]
fn bold_false_asterik() {
    TextFormat::parse_bold("**test").unwrap();
}

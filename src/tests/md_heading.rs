use super::utils::builder_header;
use crate::{ASTElement, TextFormat};

#[test]
fn heading_correct_tier_1() {
    let (rest, result) = ASTElement::parse_heading("# test").unwrap();
    assert_eq![
        result,
        builder_header(1, vec![TextFormat::Text("test".to_owned())])
    ];
    assert_eq!(rest, "");
}

#[test]
fn heading_correct_tier_2() {
    let (rest, result) = ASTElement::parse_heading("## test").unwrap();
    assert_eq![
        result,
        builder_header(2, vec![TextFormat::Text("test".to_owned())])
    ];
    assert_eq!(rest, "");
}

#[test]
fn heading_correct_tier_3() {
    let (rest, result) = ASTElement::parse_heading("### test").unwrap();
    assert_eq![
        result,
        builder_header(3, vec![TextFormat::Text("test".to_owned())])
    ];
    assert_eq!(rest, "");
}

#[test]
fn heading_correct_tier_4() {
    let (rest, result) = ASTElement::parse_heading("#### test").unwrap();
    assert_eq![
        result,
        builder_header(4, vec![TextFormat::Text("test".to_owned())])
    ];
    assert_eq!(rest, "");
}

#[test]
fn heading_correct_tier_5() {
    let (rest, result) = ASTElement::parse_heading("##### test").unwrap();
    assert_eq![
        result,
        builder_header(5, vec![TextFormat::Text("test".to_owned())])
    ];
    assert_eq!(rest, "");
}

#[test]
fn heading_correct_tier_6() {
    let (rest, result) = ASTElement::parse_heading("###### test").unwrap();
    assert_eq!(
        result,
        builder_header(6, vec![TextFormat::Text("test".to_owned())])
    );
    assert_eq!(rest, "");
}

#[test]
#[should_panic]
fn heading_without_whitespace() {
    ASTElement::parse_heading(
        "
#test",
    )
    .unwrap();
}

#[test]
#[should_panic]
fn heading_to_many_hashtags() {
    ASTElement::parse_heading("######### test").unwrap();
}

#[test]
fn heading_with_too_bold() {
    let (rest, result) = ASTElement::parse_heading("# test **test**").unwrap();
    assert_eq![
        result,
        builder_header(
            1,
            vec![
                TextFormat::Text("test ".to_owned()),
                TextFormat::Bold(vec![TextFormat::Text("test".to_owned())])
            ]
        )
    ];
    assert_eq!(rest, "");
}

#[test]
fn heading_with_too_bold_directly() {
    let (rest, result) = ASTElement::parse_heading("# **test**").unwrap();
    assert_eq![
        result,
        builder_header(
            1,
            vec![TextFormat::Bold(vec![TextFormat::Text("test".to_owned())])]
        )
    ];
    assert_eq!(rest, "");
}

use crate::{ASTElement, AST};

impl AST {
    pub fn parser(input: &str) -> AST {
        AST {
            root: ASTElement::parser(input.trim()).1,
        }
    }
}

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use CorrosionMark::md_to_html;

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("bench_markdown", |b| {
        b.iter(|| black_box(md_to_html(include_str!("test.md"))))
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);

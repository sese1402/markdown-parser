use criterion::{black_box, criterion_group, criterion_main, Criterion};
use CorrosionMark::parse_to_ast_model;

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("bench_markdown", |b| {
        b.iter(|| black_box(parse_to_ast_model(include_str!("test.md"))))
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);

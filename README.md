# My Rust Markdown Parser

This is a simple Markdown parser written in Rust. It is designed to read Markdown files and convert to diffrent file formats.


## Installation

To use this parser, add the following line to your `Cargo.toml` file:

```toml
[dependencies]
md_parser = { git = "https://gitlab.com/sese1402/markdown-parser/"}
```

## Usage 

### convert to AST Model 

```rust
use md_parser::parse_to_ASTModel;

fn main() {
    let markdown_text = "## Example\n\nThis is an **example** of using the Markdown parser.";
    let output = parse_to_ast_model(markdown_text);

    println!("{:#?}", output);
}
```

## Features

- convert to AST Model html file

